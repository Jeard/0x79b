#include <ESP32CAN.h>
#include <CAN_config.h>

//#define PCB
#if defined(PCB)
#define EN1          25
#define EN2          33
#define TX           GPIO_NUM_32
#define RX           GPIO_NUM_35
#else
#define EN1          25
#define EN2          32
#define TX           GPIO_NUM_33
#define RX           GPIO_NUM_34
#endif

#if defined (PCB)
void SelectCan(uint8_t can) {
  switch (can) {
    case 0:
      digitalWrite(EN1, HIGH);
      digitalWrite(EN2, HIGH);
      Serial.println("Can Off.");
      break;
    case 1:
      digitalWrite(EN1, HIGH);
      digitalWrite(EN2, LOW);
      Serial.println("EV Can Selected.");
      break;
    case 2:
      digitalWrite(EN1, LOW);
      digitalWrite(EN2, HIGH);
      Serial.println("Car Can Selected.");
      break;
  }
}
#else
void SelectCan(uint8_t can) {
  switch (can) {
    case 0:
      digitalWrite(EN1, HIGH);
      digitalWrite(EN2, HIGH);
      Serial.println("Can Off.");
      break;
    case 1:
      digitalWrite(EN1, LOW);
      digitalWrite(EN2, HIGH);
      Serial.println("EV Can Selected.");
      break;
    case 2:
      digitalWrite(EN1, HIGH);
      digitalWrite(EN2, LOW);
      Serial.println("Car Can Selected.");
      break;
  }
}
#endif

#define Group1_frames 6
#define Group2_frames 29
#define Group4_frames 3

volatile uint8_t group = 1;
volatile uint8_t Current_Frame = 1;
volatile uint8_t Requird_Frames = 0;
uint16_t CellPair [96];
uint16_t BatTemp[4];
uint16_t SOH;
uint32_t HaSOC;
uint32_t Cap;

uint8_t Done = 0;
uint8_t CellPairRaw[192];
uint8_t count = 0;


CAN_device_t CAN_cfg;
CAN_frame_t frame;

void SetFrameAmount() {
  switch (group) {
    case 1:
      Requird_Frames = Group1_frames;
      break;
    case 2:
      Requird_Frames = Group2_frames;
      break;
    case 4:
      Requird_Frames = Group4_frames;
      break;
  }
  Serial.print("Requird_Frames:");
  Serial.println(Requird_Frames);
}

void RequstInitialFrame(uint8_t Grp) {
  Serial.print("Request 0x79b Group:");
  Serial.println(Grp);
  frame.FIR.B.FF = CAN_frame_std;
  frame.MsgID = 0x79b;
  frame.FIR.B.DLC = 8;
  frame.data.u8[0] = 0x02;
  frame.data.u8[1] = 0x21;
  frame.data.u8[2] = Grp;
  frame.data.u8[3] = 0xff;
  frame.data.u8[4] = 0xff;
  frame.data.u8[5] = 0xff;
  frame.data.u8[6] = 0xff;
  frame.data.u8[7] = 0xff;
  ESP32Can.CANWriteFrame(&frame);
}

void RequstNextFrame() {
  frame.FIR.B.FF = CAN_frame_std;
  frame.MsgID = 0x79b;
  frame.FIR.B.DLC = 8;
  frame.data.u8[0] = 0x30;
  frame.data.u8[1] = 0x01;
  frame.data.u8[2] = 0x00;
  frame.data.u8[3] = 0xff;
  frame.data.u8[4] = 0xff;
  frame.data.u8[5] = 0xff;
  frame.data.u8[6] = 0xff;
  frame.data.u8[7] = 0xff;
  ESP32Can.CANWriteFrame(&frame);
  Serial.print("frame requested:");
  Serial.println(Current_Frame);
}

void printframe() {
  for (int i = 0; i < frame.FIR.B.DLC; i++) {
    printf("%x ", frame.data.u8[i]);
  }
  printf("\n");
}

void setup() {
  Serial.begin(115200);
  CAN_cfg.speed = CAN_SPEED_500KBPS;
  pinMode(EN1, OUTPUT);
  pinMode(EN2, OUTPUT);
  SelectCan(0);
  CAN_cfg.tx_pin_id = TX;
  CAN_cfg.rx_pin_id = RX;
  SelectCan(2);
  CAN_cfg.rx_queue = xQueueCreate(10, sizeof(CAN_frame_t));
  ESP32Can.CANInit();
  Serial.println("Initiated...");
  Serial.flush();
}

void loop() {
  while (Done != 0xff) {
    if (xQueueReceive(CAN_cfg.rx_queue, &frame, 100 * portTICK_PERIOD_MS) == pdTRUE) {
      SetFrameAmount();
      RequstInitialFrame(group);
      while (frame.MsgID != 0x7bb) {
        xQueueReceive(CAN_cfg.rx_queue, &frame, 3 * portTICK_PERIOD_MS);
      }
      switch (group) {
        case 1:
          break;
        case 2:
          for (uint8_t i = 4; i < 8; i++) {
            CellPairRaw[count] = frame.data.u8[i];
            count++;
          }
          break;
        case 4:
          BatTemp [0] = frame.data.u8[6];
          break;
      }
      Current_Frame++;
      do {
        RequstNextFrame();
        while (frame.MsgID != 0x7bb) {
          xQueueReceive(CAN_cfg.rx_queue, &frame, 10 * portTICK_PERIOD_MS);
        }

        switch (group) {
          case 1:
            if (frame.data.u8[0] == 0x24) {
              SOH = ((frame.data.u8[2] << 8) | frame.data.u8[3]);
              HaSOC = ((frame.data.u8[5] << 16) | (frame.data.u8[6] << 8) | frame.data.u8[7]);
            }
            else if ((frame.data.u8[0] == 0x25)) {
              Cap = ((frame.data.u8[2] << 16) | (frame.data.u8[3] << 8) | frame.data.u8[7]);
            }
            break;

          case 2:
            for (uint8_t i = 1; i < 8; i++) {
              while (count <= 192) {
                CellPairRaw[count] = frame.data.u8[i];
                count++;
              }
            }
            break;
          case 4:
            if (frame.data.u8[0] == 0x21) {
              BatTemp [1] = frame.data.u8[2];
              BatTemp [2] = frame.data.u8[5];
            }
            else if (frame.data.u8[0] == 0x22) {
              BatTemp [3] = frame.data.u8[1];
            }
            break;
        }
        Current_Frame++;
      } while (Current_Frame <= Requird_Frames);
      Serial.println("Frme count reset");
      Current_Frame = 1;
      switch (group){
        case 1:
        group = 2;
        break;
        case 2:
        group=4;
        break;
        case 4:
        group = 1;
        Done = 0xff;
        uint8_t j = 0;
        for (uint8_t i = 0; i <= 191; i = i + 2) {
          CellPair[j] = ((CellPairRaw[i] << 8) | CellPairRaw[i + 1]);
          j++;
        }
        printf("Cell pair info \n");
        for (uint8_t i = 0; i <= 95; i++) {
          printf("%u ", CellPair[i]);
        }
        printf("\n");
        break;
        }
        Serial.println("group increased");
        break;
      }

    }
  }
